package com.example;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties("payload")
public class Descriptor
{
    private String spoofId;
    private String sourceFile;
    private int delay;
    private byte[] payload;
    private String contentType;
    
    public byte[] getPayload()
    {
        return payload;
    }
    public void setPayload(byte[] payload)
    {
        this.payload = payload;
    }
    public String getSpoofId()
    {
        return spoofId;
    }
    public void setSpoofId(String spoofId)
    {
        this.spoofId = spoofId;
    }
    public String getSourceFile()
    {
        return sourceFile;
    }
    public void setSourceFile(String sourceFile)
    {
        this.sourceFile = sourceFile;
    }
    public int getDelay()
    {
        return delay;
    }
    public void setDelay(int delay)
    {
        this.delay = delay;
    }
    public String getContentType()
    {
        return contentType;
    }
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }
}
