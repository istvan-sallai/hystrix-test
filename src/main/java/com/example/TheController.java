package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/spoofer")
public class TheController
{
    @Autowired
    private SpoofMapper mapper;
    
    @RequestMapping(method={RequestMethod.POST,RequestMethod.GET})
    public ResponseEntity<byte[]> getSpoofResponse(@RequestParam(value = "spoofid", required = true) String spoofid)
        throws Exception
    {
        Descriptor d = mapper.get(spoofid);
        
        long delay = d.getDelay();
        if (delay > 0)
        {
            Thread.sleep(delay);
        }
        
        return new ResponseEntity<byte[]>(d.getPayload(), createHeaders(d), HttpStatus.OK);
    }
    
    private HttpHeaders createHeaders(Descriptor d )
    {
        HttpHeaders responseHeaders = new HttpHeaders();
        String contentType = d.getContentType();
        if (contentType != null)
        {
            responseHeaders.add("Content-Type", contentType);
        }
        return responseHeaders;
    }
}
