package com.example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SpoofMapper
{
    private Map<String, Descriptor> descriptors = new HashMap<>(); 

    public SpoofMapper() throws IOException
    {
        Descriptor[] temp = new ObjectMapper().readValue(
            new ClassPathResource("descriptors.json").getInputStream(), Descriptor[].class);

        for (Descriptor d : temp)
        {
             d.setPayload(IOUtils.toByteArray(new ClassPathResource("responses/" + d.getSourceFile()).getInputStream()));
             descriptors.put(d.getSpoofId(), d);
        }
    }        
    
    public Descriptor get(String spoofId) throws Exception
    {
        Descriptor d = descriptors.get(spoofId);
        if (d==null)
            throw new IllegalArgumentException("Undefined spoofId: " + spoofId);
        return d;
    }
    
    public String setDelay(String spoofId, int delay)
    {
        Descriptor d = descriptors.get(spoofId);
        if (d==null)
            throw new IllegalArgumentException("Undefined spoofId: " + spoofId);
        d.setDelay(delay);
        return "OK";
    }
}
